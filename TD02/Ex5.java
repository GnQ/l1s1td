/**
 * 
 */
package TD02;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class Ex5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub*
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrer le numéro du mois :");
		int m = sc.nextInt();
		
		System.out.println("Le mois est : ");
		switch (m) {
			case 1 : System.out.println("Janvier");
					 break;
					
			case 2 : System.out.println("Février");
					 break;
			
			case 3 : System.out.println("Mars");
					 break;
			
			case 4 : System.out.println("Avril");
					 break;
			
			case 5 : System.out.println("Mai");
					 break;
			
			case 6 : System.out.println("Juin");
					 break;
			
			case 7 : System.out.println("Juillet");
					 break;
			
			case 8 : System.out.println("Août");
					 break;
			
			case 9 : System.out.println("Septembre");
					 break;
			
			case 10 : System.out.println("Octobre");
					 break;
			
			case 11 : System.out.println("Novembre");
					  break;
			
			case 12 : System.out.println("Décembre");
					  break;
					  
			default : System.out.println("ERREUR, le numéro ne correspond pas à un mois (Entre 1 et 12)");
			
		}
	}

}
