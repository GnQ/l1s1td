/**
 * 
 */
package TD02;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD02Ex2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub*
		Scanner sc = new Scanner(System.in);
		System.out.println("Entrer le numéro du mois :");
		int m = sc.nextInt();
		
		System.out.print("Le mois est ");
		switch (m) {
			case 1 : System.out.print("Janvier");
					 break;
					
			case 2 : System.out.print("Février");
					 break;
			
			case 3 : System.out.print("Mars");
					 break;
			
			case 4 : System.out.print("Avril");
					 break;
			
			case 5 : System.out.print("Mai");
					 break;
			
			case 6 : System.out.print("Juin");
					 break;
			
			case 7 : System.out.print("Juillet");
					 break;
			
			case 8 : System.out.print("Août");
					 break;
			
			case 9 : System.out.print("Septembre");
					 break;
			
			case 10 : System.out.print("Octobre");
					 break;
			
			case 11 : System.out.print("Novembre");
					  break;
			
			case 12 : System.out.print("Décembre");
					  break;
					  
			default : System.out.print("ERREUR, le numéro ne correspond pas à un mois (Entre 1 et 12)");
			
		}
	}

}
