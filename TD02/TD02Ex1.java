/**
 * 
 */
package TD02;
import java.util.Random;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD02Ex1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("c pour casino, t pour toto : ");
		char j = sc.nextLine().charAt(0);
		switch (j) {
			case 'c', 'C' : {
				int a = (int)(Math.random()*10);
				int b = (int)(Math.random()*10);
				int c = (int)(Math.random()*10);
				System.out.println("| " + a + " | " + b + " | " + c + " |");
				if (a != b || b != c || a != c)
					System.out.println("perdu!");
				else
					System.out.println("gagné!");
				break;
			}
			case 't', 'T' : {
				Random random = new Random();

		        String st1 = "<>"; // st = string toto
		        String st2 = "-o";
		        String st3 = "|+";
		        String st4 = "-o";
		        String st5 = "<>";

		        int t1 = random.nextInt(2); // t = toto
		        char rc1 = st1.charAt(t1);  // rc = random char	        
		        int t2 = random.nextInt(2);
		        char rc2 = st2.charAt(t2);
		        int t3 = random.nextInt(2);
		        char rc3 = st3.charAt(t3);
		        int t4 = random.nextInt(2);
		        char rc4 = st4.charAt(t4);
		        int t5 = random.nextInt(2);
		        char rc5 = st5.charAt(t5);
		        
		        System.out.println(rc1 + "" + rc2 + "" + rc3 + "" + rc4 + "" + rc5);
		        
		        if (rc1 == '<' && rc2 == 'o' && rc3 == '|' && rc4 == 'o' && rc5 == '>')
		        	System.out.println("gagné!");
		        else
		        	System.out.println("perdu!");
		        break;
			}
			default : System.out.println("Mauvaise saisie.");
		}
	}

}
