/**
 * 
 */
package TD02;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD02Ex3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final int p = 300;
		Scanner sc = new Scanner(System.in);
		double L = 0;
		double l = 0;
		System.out.print("Quelle est la	forme voulue ? Saisir r pour rectangulaire, c pour cylindrique : ");
		char f = sc.nextLine().charAt(0);
		switch (f) {
			case 'r', 'R' : {
				System.out.print("Largeur en mètres : ");
				L = sc.nextDouble();
				System.out.print("Longueur en mètres : ");
				l = sc.nextDouble();
			break;
			}
			case 'c', 'C' : {
				System.out.print("Rayon en mètres : ");
				L = sc.nextDouble() * 2;
				l = L;
			break;
			}
			default : 
				System.out.println("Mauvaise saisie, veuillez réessayer.");
		}
		if (f == 'r' || f == 'R' || f == 'c' || f == 'C') {
			System.out.print("Hauteur en mètres : ");
			double h = sc.nextDouble();
			double v = L*l*h;
			double P = v*p;
			System.out.print("Prix TTC : " + P + "euros.");
		}
	}

}
