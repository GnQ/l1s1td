/**
 * 
 */
package TD02;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD02Ex5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try (// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in)) {
			double o1 = 0, o2 = 0, r = 0;
			System.out.print("Entrer l'oépration (+, -, *, /, !(opposé), i(inverse)) : ");
			char o = sc.nextLine().charAt(0);
			if (o == '+' || o == '-' || o == '*' || o == '/') {
				System.out.print("Premier nombre : ");
				o1 = sc.nextDouble();
				System.out.print("Deuxième nombre : ");
				o2 = sc.nextDouble();
				switch (o) {
					case '+' : r = o1 + o2;
					           break;
					case '-' : r = o1 - o2;
					           break;
					case '*' : r = o1 * o2;
					           break;
					case '/' : {
						if (o2 == 0)
							System.out.println("Diviser par 0 ???!");
						else
							r = o1 / o2;
				        break;
					}	
				    default : System.out.println("Comment t'as fait ton compte, même moi ça me dépasse.");
				}
			}
			else if (o == '!' || o == 'i') {
				System.out.print("Nombre : ");
				o1 = sc.nextDouble();
				switch (o) {
					case 'i' : r = 1 / o1;
					           break;
					case '!' : r = -o1;
							   break;
				}			
			}
			else
				System.out.println("Mauvaise saisie, veuillez réessayer.");
			if (o == '+' || o == '-' || o == '*' || o == '/' && o2 != 0 || o == '!' || o == 'i')
				System.out.println("Résultat : " + r);
		}
	}

}
