/**
 * 
 */
package TD02;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD02Ex6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		double x1 = 0, x2 = 0;
		System.out.print("a = ");
		double a = sc.nextDouble();
		System.out.print("b = ");
		double b = sc.nextDouble();
		System.out.print("c = ");
		double c = sc.nextDouble();
		
		if (a != 0) {
			double d = b*b-4*a*c;
			if (d <= 0) {
				if (d < 0)
					System.out.println("Pas de solution dans R");
				else {
					x1 = -b / (2*a);
					System.out.println("x = " + x1);
				}
			}
			else {
				x1 = (-b - Math.sqrt(d))/(2*a);
				x2 = (-b + Math.sqrt(d))/(2*a);
				System.out.println("Deux solutions : x1 = " + x1 + " | x2 = " + x2);
			}
		}
		else {
			x1 = -c / b;
			System.out.println("x = " + x1);
		}
	}

}
