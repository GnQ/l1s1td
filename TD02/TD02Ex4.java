/**
 * 
 */
package TD02;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD02Ex4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		double r1 = 0, i1 = 0, r2 = 0, i2 = 0, r3 = 0, i3 = 0, r4 = 0, i4 = 0, r = 0, i = 0;
		System.out.print("Entrer l'opération (+ ou - ou * ou /) : ");
		char o = sc.nextLine().charAt(0);
		if (o == '+' || o == '-' || o == '*' || o == '/') {
			System.out.print("Entrer la partie réelle du premier nombre : ");
			r1 = sc.nextDouble();
			System.out.print("Entrer la partie imaginaire du premier nombre : ");
			i1 = sc.nextDouble();
			System.out.print("Entrer la partie réelle du deuxième nombre : ");
			r2 = sc.nextDouble();
			System.out.print("Entrer la partie imaginaire du deuxième nombre : ");
			i2 = sc.nextDouble();
			switch (o) {
				case '+' : {
					r = r1 + r2;
					i = i1 + i2;
				break;
				}
				case '-' : {
					r = r1 - r2;
					i = i1 - i2;
				break;
				}
				case '*' : {
					r3 = r1 * r2;
					r4 = -1 * i1 * i2;
					i3 = r1 * i2;
					i4 = r2 * i1;
					r = r3 + r4;
					i = i3 + i4;
				break;
				}
				case '/' : {
					r3 = r1 * r2;
					r4 = i1 * i2;
					i3 = r1 * -i2;
					i4 = r2 * i1;
					r = (r3 + r4) / (r2*r2 + i2*i2);
					i = (i3 + i4) / (r2*r2 + i2*i2);
				break;
				}
			}
			if (i >= 0)
				System.out.println("Résultat : " + r + " + " + i +"i");
			else
				System.out.println("Résultat : " + r + " - " + -i +"i");
		}
		else 
			System.out.println("Mauvaise saisie, veuillez réessayer.");
		
	}

}
