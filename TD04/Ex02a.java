package TD04;
import java.util.Scanner;
public class Ex02a {

	public static void main(String[] args) {
		try (// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in)) {
			System.out.print("Entrer 3 entiers : \r\na : ");
			int a = sc.nextInt();
			System.out.print("b : ");
			int b = sc.nextInt();
			System.out.print("c : ");
			int c = sc.nextInt();
			int m = max3(a, b, c);
			System.out.print("Le maximum est : " + m);
		}
	}
	public static int max2(int a, int b) {
		if (a<b)
			return b;
		else
			return a;
	}
	public static int max3(int a, int b, int c) {
		int m = max2(a, max2(b, c));
		return m;
	}

}
