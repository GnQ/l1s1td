package TD04;

public class TD04Ex05a {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		affichetriangle(15);
		//listetriangle(15);
	}
	public static boolean triangle(int n) {
		boolean t = true;
		int i = 1;
		while (n > 0) {
			n = n-i;
			i++;
		} 
		if (n != 0)
			return false;
		else
			return true;
	}
	public static void listetriangle(int n) {
		for (int i = 1; i<=n; i++) {
			if (triangle(i))
				System.out.println(i);
		}
	}
	public static void affichetriangle(int n) {
		if (!triangle(n))
			System.out.println(n + " n'est pas triangle !");
		else
			for(int i=1; i<=n; i++) {
				for(int j=1; j<=i; j++) {
					System.out.print("x");
				}
				n = n-i;
				System.out.println("");
			}
	}
}
