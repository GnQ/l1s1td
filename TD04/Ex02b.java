package TD04;
import java.util.Scanner;
public class Ex02b {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try (Scanner sc = new Scanner(System.in)) {
			System.out.print("Entrer 3 entiers : \r\na : ");
			int a = sc.nextInt();
			System.out.print("b : ");
			int b = sc.nextInt();
			System.out.print("c : ");
			int c = sc.nextInt();
			affichage(a, b, c);
		}
	}
	public static int min2(int a, int b) {
		if (a>b)
			return b;
		else
			return a;
	}
	public static int min3(int a, int b, int c) {
		int m = min2(a, min2(b, c));
		return m;
	}
	public static int abs(int a) {
		if (a < 0)
			return -a;
		else
			return a;
	}
	public static int ecart(int a, int b) {
		int e = abs(a-b);
		return e;
	}
	public static void affichage(int a, int b, int c) {
		int mx = Ex02a.max3(a, b, c);
		System.out.println("Le maximum est : " + mx);
		int mn = min3(a, b, c);
		System.out.println("Le minimum est : " + mn);
		int e = ecart(mn, mx);
		System.out.println("L'�cart est : " + e);
	}
}
