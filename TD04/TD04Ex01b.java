package TD04;
import java.util.Scanner;
public class TD04Ex01b {
	
	public static void main(String[] args) {
		try (// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in)) {
			System.out.print("a = ");
			int a = sc.nextInt();
			System.out.print("b = ");
			int b = sc.nextInt();
			System.out.print("c = ");
			int c = sc.nextInt();
			if (congruence(a, b, c) == true)
				System.out.println(a + " est congru à " + b + " modulo " + c + ".");
			else
				System.out.println(a + " n'est pas congru à " + b + " modulo " + c + ".");
		}
	}
	public static boolean congruence(int a, int b, int c) {
		if (a%c == b%c)
			return true;
		else
			return false;
	}
}
