package TD04;

public class TD04Ex03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.print(testEgalite(1, 1));
		suite();
	}
	public static boolean testEgalite(double d1, double d2) {
		d1 = 4.5/(float)6.3;
		d2 = (4.5/(float)2.1)/3;
		if (abs(d1-d2)<0.00001)
			return true;
		else 
			return false;
	}
	public static double abs(double d) {
		if (d<0)
			return -d;
		else
			return d;
	}
	
	//Pour savoir si une suite converge, on compare � chaque it�ration le terme obtenu au terme pr�c�dent. Si la diff�rence � un certain seuil est nulle, on peut affirmer que la suite converge en c = abs(dernier terme - terme pr�c�dent)
	
	public static void suite() {
		double dt = 0;
		double tp = 1;
		int i = 1;
		while (!testEgalite(dt, tp)) {
			dt += (1/2^i);
			tp = dt - (1/2^i);
			i++;
			if (i>10000) {
				System.out.println("La suite ne converge pas.");
				break;
			}
		}
		if (testEgalite(dt,tp))
			System.out.println("La suite converge en " + abs(dt-tp));
	}
}
