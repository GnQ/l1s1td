package TD04;

public class TD04Ex04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public static boolean premier(int n) {
		for(int i = 2; i<n; i++) {
			if(i%n == 0) {
				return false;
			}
		}
		return true;
	}
	public static void listepremier(int n) {
		for (int i = 1; i<=n; i++) {
			if (premier(i))
			System.out.println(i);
		}
	}
	public static boolean parfait(int n) {
		int s = 0;
		for (int i = 1; i<n; i++) {
			if (n%i == 0) {
				s += i;
			}
		}
		if (s != n)
			return false;
		return true;
	}
	public static void listeparfait(int n) {
		for (int i = 1; i<n; i++) {
			if (parfait(i))
				System.out.println(i);
		}
	}

}
