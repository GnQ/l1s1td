package TD04;
import java.util.Scanner;
public class Ex01 {

	public static void main(String[] args) {
		try (// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in)) {
			System.out.print("Entrer un entier : ");
			int a = sc.nextInt();
			long x = factorielle(a);
			System.out.println(a + "! = " + x);
		}
	}
	public static long factorielle(int a) {
		long b = a;
		for (int i = 1; i<a; i++) {
			b *= (a-i);
		}
		return b;
	}
}
