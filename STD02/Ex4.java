/**
 * 
 */
package SoutineTD02;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class Ex4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("n = ");
		int n = sc.nextInt();
		int n2 = n;
		System.out.print("n! = " + n + "! = " + n);
		for (int i = 1; i < n; i++) {
			n2 *= (n-i) ;
			System.out.print("*" + (n-i));
		}
		System.out.print(" = " + n2);
	}

}
