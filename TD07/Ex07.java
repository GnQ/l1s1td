package TD07;
import java.util.Scanner;
public class Ex07 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Bienvenue au marché de Padi-Pado, que recherchez-vous ? ");
		String obj = sc.nextLine();
		if(padiPado(obj))
			System.out.print("\r\nCertainement, veuillez me suivre.");
		else
			System.out.print("\r\nPassez votre chemin, pas de ça ici.");
	}
	public static boolean padiPado(String ch) {
		if(ch.indexOf('i') >= 0 || ch.indexOf('o') >= 0)
				return false;
		return true;
	}
}
