package TD07;
import java.util.Scanner;
public class TD07Ex02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Chaîne à décorer : ");
		String dcor = sc.nextLine();
		System.out.print("Nombre d'étoiles : ");
		int toiles = sc.nextInt();
		System.out.println(ajouteEtoiles(dcor, toiles));
	}
	public static String ajouteEtoiles(String ch, int n) {
		String toile = "";
		for (int i=0; i<n; i++)
			toile += "*";
		return toile + ch + toile;
	}
}
