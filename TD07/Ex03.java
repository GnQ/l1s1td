package TD07;
import java.util.Scanner;
public class Ex03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("1ère chaîne : ");
		String ch1 = sc.nextLine();
		System.out.print("2e chaîne : ");
		String ch2 = sc.nextLine();
		alpha(ch1, ch2);
	}
	public static void alpha(String ch1, String ch2) {
		if (ch1.compareTo(ch2)<0)
			System.out.print("\r\n" + ch1 + "\r\n" + ch2);
		else if (ch1.compareTo(ch2)>0)
			System.out.print("\r\n" + ch2 + "\r\n" + ch1);
		else 
			System.out.print("Les deux chaînes sont identiques (\"" + ch1 + "\").");
	}
}
