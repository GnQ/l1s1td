package TD07;
import java.util.Scanner;
public class Ex06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Entrer l'heure sous forme HH:MM:SS : ");
		String ch = sc.nextLine();
		if(heureCheck(ch)) {
			System.out.print(ch.substring(0,2) + " " + heure(ch) + " " + ch.substring(3,5) + " " + minute(ch) + " " + ch.substring(6,8) + " " + seconde(ch));
		}
		else
			System.out.println("Veuillez entrer le format demandé.");
	}
	public static boolean heureCheck(String ch) {
		if (ch.length() != 8 || ch.charAt(2) != ':' || ch.charAt(5) != ':') {
			return false;
		}
		else return true;
	}
	public static String heure(String ch) {
		if(ch.substring(0,2).compareTo("00") == 0 || ch.substring(0,2).compareTo("01") == 0)
			return "heure";
		else 
			return "heures";
	}
	public static String minute(String ch) {
		if(ch.substring(3,5).compareTo("00") == 0 || ch.substring(3,5).compareTo("01") == 0)
			return "minute";
		else 
			return "minutes";
	}
	public static String seconde(String ch) {
		if(ch.substring(6,8).compareTo("00") == 0 || ch.substring(6,8).compareTo("01") == 0)
			return "seconde";
		else 
			return "secondes";
	}
}
