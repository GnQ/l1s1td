package TD07;
import java.util.Scanner;
public class TD07Ex06DemoString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}
	public void test() {
		System.out.print("Fonctionnes-tu ?");
	}
	public void saisie() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Saisir une chaîne de caractres : ");
		String s = sc.nextLine();
		System.out.println(s);
	}
	public void affiche(String s) {
		System.out.println("Chaîne reçue : " + s + "; Longueur totale : " + (s.length()+1));
	}
	public void affiVertical(String s) {
		for (int i=0; i<s.length(); i++) {
			System.out.println(s.charAt(i));
		}
	}
	public int compteCar(String s, char c) {
		int n = 0;
		for (int i=0; i<s.length(); i++) {
			if (s.charAt(i)==c)
				n+=1;
		}
		return n;
	}
	public int compteVoy(String s) {
		int n = 0;
		for (int i=0; i<s.length(); i++) {
			if (s.charAt(i)=='a' || s.charAt(i)=='e' || s.charAt(i)=='i' || s.charAt(i)=='o' || s.charAt(i)=='u' || s.charAt(i)=='y')
				n+=1;
		}
		return n;
	}
	public String remplace(String s, char a, char b) {
		String[] n = s.split(Character.toString(a));
		String ns = "";
		for (int i=0; i<n.length-1; i++) {
			ns += n[i] + b;
		}
		ns+= n[n.length-1];
		return ns;
	}
	public boolean dans(String e, String s) {
		if(s.indexOf(e) == -1)
			return false;
		else
			return true;
	}
}
