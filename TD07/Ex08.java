package TD07;

import java.util.Scanner;

public class Ex08 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Entrer l'heure sous forme HH:MM:SS : ");
		String ch = sc.nextLine();
		String[] paf = ch.split(":");
		if(heureCheck(paf)) {
			System.out.print(paf[0] + " " + heure(paf) + " " + paf[1] + " " + minute(paf) + " " + paf[2] + " " + seconde(paf));
		}
		else
			System.out.println("Veuillez entrer le format demandé.");
	}
	public static boolean heureCheck(String[] ch) {
		if (ch.length != 2) {
			if (ch[0].length() != 2 || ch[1].length() != 2 || ch[2].length() != 2) {
				return false;
			}
			else 
				return true;
		}
		else return false;
	}
	public static String heure(String[] ch) {
		if(ch[0].compareTo("00") == 0 || ch[0].compareTo("01") == 0)
			return "heure";
		else 
			return "heures";
	}
	public static String minute(String[] ch) {
		if(ch[1].compareTo("00") == 0 || ch[1].compareTo("01") == 0)
			return "minute";
		else 
			return "minutes";
	}
	public static String seconde(String[] ch) {
		if(ch[2].compareTo("00") == 0 || ch[2].compareTo("01") == 0)
			return "seconde";
		else 
			return "secondes";
	}
}
