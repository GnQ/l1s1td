package TD07;

public class TD07Ex07 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println(estPalindrome("ava"));
		//System.out.println(estPalindrome("owwwww"));
		//System.out.println(inverse("rabelais"));
		//System.out.println("aaa".compareToIgnoreCase("Aaa"));
		System.out.println(estPalindrome2("ava"));
		System.out.println(estPalindrome2("owwwww"));
	}
	public static boolean estPalindrome(String s) {
		for (int i=0; i<((s.length()-1)/2); i++) {
			if (s.charAt(i) != s.charAt(s.length()-1-i))
				return false;
		}
		return true;
	}
	public static String inverse(String s) {
		String d = "";
		for (int i=s.length()-1; i>=0; i--) {
			d += s.charAt(i);
		}
		return d;
	}
	public static boolean estPalindrome2(String s) {
		if(s.compareToIgnoreCase(inverse(s)) != 0)
			return false;
		else
			return true;
	}
}
