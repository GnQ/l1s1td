/**
 * 
 */
package SoutineTD01;

/**
 * @author Keziah
 *
 */
public class Ex5b {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i = 0;
		int s = 0;
		
		System.out.println("Entiers compris entre 1 et 10 divisibles par 3 ou 5");
		for (i=1; i<=10; i++) {
			//d = i + 1;
			if (i%5 == 0 && i%3 == 0) {
				s += i;
				System.out.println(i);
			}
		}
		System.out.println("Somme = " + s);
	}

}
