package TD08;

public class TD08Ex01Compteur {

	private int val;
	
	public TD08Ex01Compteur() {
		val = 0;
	}
	
	public void Clic() {
		val ++;
	}
	
	public void raz() {
		val = 0;
	}
	
	public int getValeur() {
		return val;
	}
}
