package SoutineTD03;
import java.util.Scanner;
public class STD03Ex03 {

	public static void main(String[] args) {
		try (// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in)) {
			float txRemise;
			System.out.println("Veuillez saisir le montant de votre facture ");
			float montantFacture = sc.nextFloat();
			txRemise = tauxDeRemise(montantFacture); // appel de la fonction
			System.out.println("Le taux de remise  appliquer est :"+txRemise);
			float mtRemise = montantFacture*txRemise;
			montantFacture -= mtRemise;
			System.out.println("Le montant de la remise est :"+mtRemise);
			System.out.println("Le montant de la facture après remise est :"+montantFacture);
		}
		}
	public static float tauxDeRemise(float totalFact)
	{
	float taux;
	if (totalFact<10000) {
	taux= 0.1f ;
	}
	else {
	if (totalFact<20000) {
	taux=0.2f ;
	}
	else {
	taux=0.3f ;
	}
	}
	return (taux) ; // obligatoire dès qu'on a une fonction
	}

}
