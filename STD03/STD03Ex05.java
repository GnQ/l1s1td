package SoutineTD03;
import java.util.Scanner;
public class STD03Ex05 {

	public static void main(String[] args) {
		try (// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in)) {
			float a = 0, b = 0, c = 0, n = 0;
			for (int i = 1; i <= 3; i++) {
				System.out.print("Note n°" + i + " : ");
				n = sc.nextFloat();
				if (n < 0) {
					System.out.println("Ne te sous-estime pas !");
					i--;
				}
				else if (n > 20) {
					System.out.println("N'abuse pas.");
					i--;
				}
				else {
					if (i == 1)
						a = n;
					if (i == 2)
						b = n;
					if (i == 3)
						c = n;
				}
			}
			float m = moyenne3nombres(a, b, c);
			System.out.println("Moyenne : " + m + "/20.");
			afficheResultatSemestre(m);
		}
		
	}
	public static float moyenne3nombres(float a, float b, float c) {
		float m = (a + b + c)/3;
		return m;
	}
	public static void afficheResultatSemestre(float moy) {
		if(moy < 8)
			System.out.println("A l'année prochaine, peut-être");
		if(moy >= 8 && moy < 10)
			System.out.println("Encore quelques efforts");
		if(moy >= 10 && moy < 14)
			System.out.println("Bonnes vacances");
		if(moy >= 14)
			System.out.println("Très bonnes vacances");
	}

}
