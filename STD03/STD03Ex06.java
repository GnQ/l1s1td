package SoutineTD03;
import java.util.Scanner;
public class STD03Ex06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		for (int i = 1; i<=3; i++) {
			float n = nbDsIntervalle(1, 5);
			if ((3-i) > 1)
				System.out.println("Vous avez saisi " + n + ", il vous reste " + (3-i) + " nombres � saisir.");
			else
				System.out.println("Vous avez saisi " + n + ", il vous reste " + (3-i) + " nombre � saisir.");
		}
	}
	public static float nbDsIntervalle(int borne1, int borne2) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Entrez un nombre compris entre " + borne1 + " et " + borne2 + " : ");
		float nb = sc.nextFloat();
		while (borne1 > nb || borne2 < nb) {
			System.out.print("Mauvaise saisie. R�essayez. Le nombre doit �tre compris entre " + borne1 + " et " + borne2 + " : ");
			nb = sc.nextFloat();
		}
			return nb;
	}
}
