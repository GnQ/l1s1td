package SoutineTD03;
import java.util.Scanner;
public class STD03Ex04 {

	public static void main(String[] args) {
		try (// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in)) {
			System.out.print("Saisir un nombre : ");
			float n = sc.nextFloat();
			n = doubleDunNombre(n);
			System.out.println("Son double est : " + n + ".");
		}
	}
	 public static float doubleDunNombre(float nb) {
		 nb *= 2;
		 return nb;
	 }
}
