package SoutineTD03;
import java.util.Scanner;
public class STD03Ex08b{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		STD03Ex08a p = new STD03Ex08a();
		System.out.print("Nombre : ");
		int nb = sc.nextInt();
		System.out.println("Surface terrain : " + p.puissance(nb, 2));
		System.out.println("Surface cube : " + p.puissance(nb, 3));
	}
	public int puissance(int nb, int exposant) {
		int n = nb;
		for (int i = 2; i<=exposant; i++) {
			nb *= n;
		}
		return nb;
	}
}
