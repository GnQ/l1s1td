package SoutineTD03;
import java.util.Scanner;
public class STD03Ex02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print("Entrer la température de l'eau (en degrés Celsius) : ");
		try (Scanner sc = new Scanner(System.in)) {
			float t = sc.nextFloat();
			afficheEtatEau(t);
		}
	}
	 public static void afficheEtatEau(float temp) {
		 if (temp < 0)
			 System.out.println("Glace");
		 if (temp > 0 && temp < 100)
			 System.out.println("Liquide");
		 if (temp > 100)
			 System.out.println("Vapeur");
	 }
}
