package TD06;

public class TD06Ex02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] mat1 = initMat(3, 4, 20);
		int[][] mat2 = initMat(3, 4, 25);
		int[][] mat3 = additionner(mat1, mat2);
		printMat(mat1);
		System.out.println("");
		printMat(mat2);
		System.out.println("");
		printMat(mat3);
		
	}
	public static int[][] initMat(int n, int m, int max) {
		int[][] mat = new int[n][m];
		for (int i=0; i<mat.length; i++) {
			for (int j=0; j<mat[0].length; j++) {
				mat[i][j] = (int)(Math.random()*(max+1));
			}
		}
		return mat;
	}
	public static void printMat(int mat[][]) {
		for (int i=0; i<mat.length; i++) {
			for (int j=0; j<mat[0].length; j++) {
				System.out.print(mat[i][j] + " ");
			}
			System.out.println("");
		}
	}
	public static int matMax(int[][] mat) {
		int max = mat[0][0];
		for (int i=0; i<mat.length; i++) {
			for(int j=0; j<mat.length; j++) {
				if(mat[i][j]>max)
					max = mat[i][j];
			}
		}
		return max;
	}
	public static int[] vectLigneMax(int[][] mat) {
		int[] mattention = new int[mat.length];
		int maxl = 0;
		for (int i=0; i<mat.length; i++) {
			for (int j=0; j<mat[0].length; j++) {
				if (mat[i][j] > maxl) {
					maxl = mat[i][j];
				}
				mattention[i] = maxl;
				maxl = 0;
			}
		}
		return mattention;
	}
	public static int[] vectColonneMax(int[][] mat) {
		int[] mattriste = new int[mat[0].length];
		int maxc = 0;
		for (int i=0; i<mat[0].length; i++) {
			for (int j=0; j<mat.length; j++) {
				if (mat[i][j] > maxc) {
					maxc = mat[i][j];
				}
				mattriste[i] = maxc;
				maxc = 0;
			}
		}
		return mattriste;
	}
	public static int[][] additionner(int[][] mat1,int[][] mat2){
		int[][] mat3 = new int[mat1.length][mat1[0].length];
		for (int i=0; i<mat3.length; i++) {
			for (int j=0; j<mat3[0].length; j++) {
				mat3[i][j] = mat1[i][j] + mat2[i][j];
			}
		}
		return mat3;
	}
}
