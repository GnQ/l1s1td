package TD06;

public class TD06Ex01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public static int[] iniTab(int n, int b) {
		int[] tab = new int[n];
		for (int i=0; i<tab.length; i++) {
			tab[i] = (int)(Math.random()*b);
		}
		return tab;
	}
	public static void printabInt(int[] tab) {
		for (int i=0; i<tab.length; i++) {
			System.out.print(tab[i] + " ");
		}
	}
	public static int maxTab(int[] tab) {
		int max = tab[0];
		for (int i=1; i<tab.length; i++) {
			if(tab[i]>max)
				max = tab[i];
		}
		return max;
	}
	public static int[] cardTab(int[] tab) {
		int c = 0;
		int[] card = new int[maxTab(tab)];
		for (int i=0; i<card.length; i++) {
			for (int j=0; j<tab.length; j++) {
				if (tab[j] == i)
					c++;
			}
			card[i] = c;
			c = 0;
		}
		return card;
	}
	public static int modeTab(int[] tab) {
		int c = 0, cm = 0, m = 0;
		for (int i=0; i<maxTab(tab); i++) {
			for (int j=0; j<tab.length; j++) {
				if (tab[j] == i)
					c++;
					if (c > cm) {
						cm = c;
						m = tab[j];
					}
			}
			c = 0;
		}
		return m;
	}
}
