/**
 * 
 */
package TD01;
import java.util.Scanner;

/**
 * @author Keziah
 *
 */
public class TD01Ex6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a, b, c, t;
		String mn, i, mx, t2;
		mn = "a";
		i = "b";
		mx = "c";
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Saisir a :");
        a = sc.nextInt();
        System.out.println("Saisir b :");
        b = sc.nextInt();
        System.out.println("Saisir c :");
        c = sc.nextInt();
        
        if (b<a) {
            t = a;
            a = b;
            b = t;
            t2 = mn;
            mn = i;
            i = t2;
        }
        if (c<b) {
            t = b;
            b = c;
            c = t;
            t2 = i;
            i = mx;
            mx = t2;
        }
        if (b<a) {
            t = a;
            a = b;
            b = t;
            t2 = mn;
            mn = i;
            i = t2;
        }
        System.out.println(a + " (" + mn + ") < " + b + " (" + i + ") < " + c + " (" + mx + ")");
    }

}
