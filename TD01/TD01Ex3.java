/**
 * 
 */
package TD01;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD01Ex3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n, d, q, r;
		Scanner sc = new Scanner(System.in);
		System.out.println("Dividende : ");
		n = sc.nextInt();
		System.out.println("Diviseur : ");
		d = sc.nextInt();
		
		if(d != 0) {
			q = n/d;
			r = n%d;
		
			System.out.println("Quotient : " + q + ", Reste : " + r);
		}
		else System.out.println("Division par 0 impossible !");

	}

}
