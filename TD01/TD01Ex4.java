/**
 * 
 */
package TD01;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD01Ex4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a, u, d, c;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Saisir un entier inférieur à 1000");
		a = sc.nextInt();
		
		if(a < 1000) {
			u = a%10;
			d = (a%100)/10;
			c = (a%1000)/100;
			
			if(c != 0) {
				System.out.println("Centaines : " + c);
			}
			if(d != 0) {
				System.out.println("Dizaines : " + d);
			}
			System.out.println("Unités : " + u);
			
		}
		else System.out.println("Inférieur à 1000 j'ai dit.");

	}

}
