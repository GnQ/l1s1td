/**
 * 
 */
package TD01;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD01Ex8 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Quelle année tester ?");
		a = sc.nextInt();
		
		if ((a%4 == 0 && !(a%100 == 0)) || a%400 == 0) {
			System.out.println("L'année " + a + " est bissextile.");
		}
		else
			System.out.println("L'année " + a + " n'est pas bissextile.");

	}

}
