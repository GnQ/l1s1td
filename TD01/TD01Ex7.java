/**
 * 
 */
package TD01;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD01Ex7 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int t, h, m, s;
		String h1 = " heures";
		String m1 = " minutes";
		String s1 = " secondes";
		String s2 = " secondes = ";
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Entrer un temps en secondes :");
		t = sc.nextInt();
		
		h = t/3600;
		m = (t%3600)/60;
		s = (t%3600)%60;
		
		if(h == 1) {
			h1 = " heure";
		}
		if(m == 1) {
			m1 = " minute";
		}
		if(s == 1 || s == 0) {
			s1 = " seconde";
		}
		if(t == 1 || t == 0) {
			s2 = " seconde = ";
		}
		
		if(s != 0) {
			if(h == 0) {
				if (m == 0) {
				System.out.println(t + s2 + s + s1 + " (Logique)");
				}
				else 
					System.out.println(t + s2 + m + m1 + " et " + s + s1);
			}
			else 
				System.out.println(t + s2 + h + h1 + ", " +  m + m1 + " et " + s + s1);
		}
		else {
			if (h == 0) {
				if (m == 0) {
					System.out.println(t + s2 + s + s1 + " (Logique)");
					}
				else 
					System.out.println(t + s2 + m + m1);	
			}
			else {
				if (m == 0) {
					System.out.println(t + s2 + h + h1);
				}
				else
					System.out.println(t + s2 + h + h1 + " et " +  m + m1);
			}
		}
	}

}
