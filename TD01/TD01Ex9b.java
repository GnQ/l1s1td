/**
 * 
 */
package TD01;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD01Ex9b {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n;
		double p, mt1, mr, mt2, tva, r;
		tva = 0.196;
		r = 0;
		mr = 0;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Nombre d'articles :");
		n = sc.nextInt();
		System.out.println("Prix unitaire :");
		p = sc.nextDouble();
		
		mt1 = (n*p)*(1+tva);
		mt2 = mt1;
		
		if (200 <= mt1 && mt1 < 500)
			r = 0.05;
		if (500 <= mt1 && mt1 < 1000)
			r = 0.07;
		if (1000 <= mt1)
			r = 0.1;
		
		mr += r*mt1;
		mt2 -= mr;
		
		System.out.println("FACTURE \r\nNb darticles : " + n + "\r\nPrix unitaire H.T. en euros : " + p + "\r\nMontant T.T.C. en euros : " + mt1 + "\r\nMontant de la remise : " + mr + "\r\nMontant après la remise : " + mt2);

	}

}
