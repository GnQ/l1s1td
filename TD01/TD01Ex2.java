/**
 * 
 */
package TD01;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD01Ex2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double l, L, h, e, f, g, S, V;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Longueur = ");
		l = sc.nextDouble();
		System.out.println("Largeur = ");
		L = sc.nextDouble();
		System.out.println("Hauteur = ");
		h = sc.nextDouble();
		
		e = l*L;
		f = L*h;
		g = l*h;
		
		S = 2*e + 2*f + 2*g;
		V = L*l*h;
		
		System.out.println("La surface du parallélépipède est " + S + " cm2");
		System.out.println("Le volume du parallélépipède est " + V + " cm3");
	}

}
