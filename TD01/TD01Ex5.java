/**
 * 
 */
package TD01;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD01Ex5 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Saisir x : ");
		x = sc.nextInt();
		
		if(x%2 == 0) {
			System.out.println("x (" + x + ") est pair.");
		}
		else System.out.println("x (" + x + ") est impair.");

	}

}
