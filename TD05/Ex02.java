package TD05;

public class Ex02 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double[] tab = {67, 56478, -654, 90, 2};
		System.out.println("Le maximum est : " + maxtab(tab));
	}
	public static double maxtab(double[] tab) {
		double max = tab[0];
		for (int i=1; i<tab.length; i++) {
			if (tab[i] > max)
				max = tab[i];
		}
		return max;
	}
}
