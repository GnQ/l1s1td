package TD05;

import java.util.Scanner;

public class Ex06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Nombre de lignes : ");
		int l = sc.nextInt();
		System.out.print("Nombre de colonnes : ");
		int L = sc.nextInt();
		int[][] mat = new int[l][L];
		for (int i=0; i<mat.length; i++) {
			for (int j=0; j<mat[0].length; j++) {
				mat[i][j] = sc.nextInt();
			}
		}
		Ex03.printmat(mat);
		System.out.println("\r\n");
		annulmat(mat);
		Ex03.printmat(mat);
	}
	public static void annulmat(int[][] mat) {
		for(int i=0; i<mat.length; i++) {
			for(int j=0; j<mat[0].length; j++) {
				mat[i][j] = 0;
			}
		}
	}
}
