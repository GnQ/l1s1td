package TD05;
import java.util.Scanner;
public class Ex03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int[][] tab = new int[3][3];
		for (int i=0; i<3; i++) {
			for (int j=0; j<3; j++) {
				tab[i][j] = sc.nextInt();
			}
		}
		printmat(tab);
	}
	public static void printmat(int tab[][]) {
		for (int i=0; i<tab.length; i++) {
			for (int j=0; j<tab[0].length; j++) {
				System.out.print(tab[i][j] + " ");
			}
			System.out.println("");
		}
	}
}