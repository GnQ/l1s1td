package TD05;

import java.util.Scanner;

public class Ex07 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Nombre de lignes : ");
		int l = sc.nextInt();
		System.out.print("Nombre de colonnes : ");
		int L = sc.nextInt();
		int[][] mat = new int[l][L];
		for (int i=0; i<mat.length; i++) {
			for (int j=0; j<mat[0].length; j++) {
				mat[i][j] = sc.nextInt();
			}
		}
		Ex03.printmat(mat);
		System.out.println("\r\n");
		int[] tab = sommeMat(mat);
		printabInt(tab);
	}
	public static int[] sommeMat(int[][] mat) {
		int s = 0;
		int[] tab = new int[mat.length];
		for(int i=0; i<mat.length; i++) {
			for(int j=0; j<mat[0].length; j++) {
				s += mat[i][j];
			}
			tab[i] = s;
			s = 0;
		}
		return tab;
	}
	public static void printabInt(int[] tab) {
		for (int i=0; i<tab.length; i++) {
			System.out.print(tab[i] + " ");
		}
	}
}
