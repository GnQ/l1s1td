package TD05;
import java.util.Scanner;
public class Ex04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Taille du tableau : ");
		int l = sc.nextInt();
		System.out.print("Seuil : ");
		int s = sc.nextInt();
		double tab[] = new double[l];
		for(int i=0; i<l; i++) {
			tab[i] = sc.nextDouble();
		}
		int n = supSeuil(tab, s);
		System.out.println(n);
	}
	public static int supSeuil(double[] tab, int seuil) {
		int nb = 0;
		for (int i = 0; i<tab.length; i++) {
			if (tab[i]>seuil)
				nb += 1;
		}
		return nb;
	}
}
