/**
 * 
 */
package Improv;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD01Ex9bonus {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n1, i, j, nt;
		double p1, mr, mt1, mt2, tva, r, pt;
		tva = 0.196;
		r = 0;
		mr = 0;
		mt1 = 0;
		nt = 0;
		pt = 0;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Types d'articles différents :");
		j = sc.nextInt();
		
		
		if (j>1) {
			int[] n2 = new int[j] ;
			double[] p2 = new double[j];
			for (i=0; i<j; i++) {
				System.out.println("ARTICLE N°" + (i+1) + " : \r\nNombre d'articles : ");
				n2[i] = sc.nextInt();
				System.out.println("Prix unitaire : ");
				p2[i] = sc.nextDouble();
				
				mt1 += (n2[i]*p2[i]);
				nt += n2[i];
				pt += p2[i];
			}
			mt1 *= (1+tva);
			mt2 = mt1;
			
			if (200 <= mt1 && mt1 < 500)
			r = 0.05;
			if (500 <= mt1 && mt1 < 1000)
			r = 0.07;
			if (1000 <= mt1)
				r = 0.1;
	
			mr += r*mt1;
			mt2 -= mr;
			
			System.out.println("FACTURE");
			for (i=0; i<j; i++)
				System.out.println("\r\nArticle n°" + (i+1) + "\r\nNb d'articles : " + n2[i] + "\r\nPrix unitaire H.T. en euros : " + p2[i]);
			System.out.println("\r\nNombre Total d'articles : " + nt + "\r\nPrix Total H.T. : " + pt + "\r\nMontant T.T.C. en euros : " + mt1 + "\r\nMontant de la remise : " + mr + "\r\nMontant après la remise : " + mt2);
		}
		else {
			if (j == 1) {
				System.out.println("Nombre d'articles :");
				n1 = sc.nextInt();
				System.out.println("Prix unitaire :");
				p1 = sc.nextDouble();
				
				mt1 = (n1*p1)*(1+tva);
				mt2 = mt1;
		
				if (200 <= mt1 && mt1 < 500)
				r = 0.05;
				if (500 <= mt1 && mt1 < 1000)
				r = 0.07;
				if (1000 <= mt1)
					r = 0.1;
		
				mr += r*mt1;
				mt2 -= mr;
		
				System.out.println("FACTURE \r\nNb d'articles : " + n1 + "\r\nPrix unitaire H.T. en euros : " + p1 + "\r\nMontant T.T.C. en euros : " + mt1 + "\r\nMontant de la remise : " + mr + "\r\nMontant après la remise : " + mt2);
			}
			if (j == 0)
				System.out.println("Pas d'achat, pas de facture !");
				
		}
	}

}
