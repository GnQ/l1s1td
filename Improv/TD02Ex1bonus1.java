/**
 * 
 */
package Improv;
import java.util.Random;
import java.util.Scanner;

/**
 * @author Keziah
 *
 */
public class TD02Ex1bonus1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a = 0, b = 1, c = 2, i = 0;
		char rc1 = 'a', rc2 = 'b', rc3 = 'c', rc4 = 'd', rc5 = 'e';
		Scanner sc = new Scanner(System.in);
		System.out.print("c pour casino, t pour toto : ");
		char j = sc.nextLine().charAt(0);
		switch (j) {
			case 'c', 'C' : {
				while (a != b || b != c || a != c) {
						a = (int)(Math.random()*10);
						b = (int)(Math.random()*10);
						c = (int)(Math.random()*10);
						i += 1;
				}
				if (i == 1)
					System.out.println("| " + a + " | " + b + " | " + c + " | \r\nVictoire en " + i + " coup.");
				else
					System.out.println("| " + a + " | " + b + " | " + c + " | \r\nVictoire en " + i + " coups.");
				break;
			}
			case 't', 'T' : {
				Random random = new Random();

		        String st1 = "<>"; // st = string toto
		        String st2 = "-o";
		        String st3 = "|+";
		        String st4 = "-o";
		        String st5 = "<>";
		        
		        while (rc1 != '<' || rc2 != 'o' || rc3 != '|' || rc4 != 'o' || rc5 != '>') {
		        	int t1 = random.nextInt(2); // t = toto
		        	rc1 = st1.charAt(t1);  // rc = random char	        
		       		int t2 = random.nextInt(2);
		       		rc2 = st2.charAt(t2);
		       		int t3 = random.nextInt(2);
		       		rc3 = st3.charAt(t3);
		       		int t4 = random.nextInt(2);
		       		rc4 = st4.charAt(t4);
		       		int t5 = random.nextInt(2);
		       		rc5 = st5.charAt(t5);
	        		i += 1;
		        }
		        if (i == 1)
		        	System.out.println(rc1 + "" + rc2 + "" + rc3 + "" + rc4 + "" + rc5 + " Victoire en " + i + " coup.");
		        else
		        	System.out.println(rc1 + "" + rc2 + "" + rc3 + "" + rc4 + "" + rc5 + " Victoire en " + i + " coups.");
		        break;
			}
			default : System.out.println("Mauvaise saisie.");
		}
	}

}
