/**
 * 
 */
package Improv;
import java.util.Random;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class STD02Ex7bonus1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int r = (int)(Math.random()*100), i = 0, g = -1;
		System.out.print("Essaie de deviner le nombre que j'ai choisi. Il est compris entre 1 et 100.");
		while (r != g) {
			System.out.print("\r\nGuess n" + (i+1) + " ? : ");
			i += 1;
			g = sc.nextInt();
			if (r > g)
				System.out.print("Trop petit !");
			else if (g>r)
				System.out.print("Trop grand !");
		}
		if (i != 1)
			System.out.print("Bien joué, le nombre était " + r + "\r\nVictoire en " + i + " coups");
		else
			System.out.print("Bien joué, le nombre était " + r + "\r\nVictoire en " + i + " coup");
	}

}
