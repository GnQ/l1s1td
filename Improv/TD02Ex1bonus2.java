/**
 * 
 */
package Improv;
import java.util.Random;
import java.util.Scanner;

/**
 * @author Keziah
 *
 */
public class TD02Ex1bonus2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a = 0, b = 1, c = 2, i = 0, q = 0, d = 0;
		double m;
		char rc1 = 'a', rc2 = 'b', rc3 = 'c', rc4 = 'd', rc5 = 'e';
		Scanner sc = new Scanner(System.in);
		System.out.print("c pour casino, t pour toto : ");
		char j = sc.nextLine().charAt(0);
		System.out.print("Moyenne sur combien ? --> ");
		int qe = sc.nextInt();
		switch (j) {
			case 'c', 'C' : {
				while (q < qe) {
					while (a != b || b != c || a != c) {
						a = (int)(Math.random()*10);
						b = (int)(Math.random()*10);
						c = (int)(Math.random()*10);
						i += 1;
					}
					q += 1;
					d += i;
					i = 0;
					a = 0;
					b = 1;
					c = 2;
				}
				m = d/q;
				System.out.println("Victoire en " + m + " coups en moyenne.");
				break;
			}
			case 't', 'T' : {
				Random random = new Random();

		        String st1 = "<>"; // st = string toto
		        String st2 = "-o";
		        String st3 = "|+";
		        String st4 = "-o";
		        String st5 = "<>";
		        
		        while (q < qe) {
		        	while (rc1 != '<' || rc2 != 'o' || rc3 != '|' || rc4 != 'o' || rc5 != '>') {
		        		int t1 = random.nextInt(2); // t = toto
		        		rc1 = st1.charAt(t1);  // rc = random char	        
		        		int t2 = random.nextInt(2);
		        		rc2 = st2.charAt(t2);
		        		int t3 = random.nextInt(2);
		        		rc3 = st3.charAt(t3);
		        		int t4 = random.nextInt(2);
		        		rc4 = st4.charAt(t4);
		        		int t5 = random.nextInt(2);
		        		rc5 = st5.charAt(t5);
		        		i += 1;
		        	}
		        	q += 1;
					d += i;
					i = 0;
					rc1 = 'a';
					rc2 = 'b';
					rc3 = 'c';
					rc4 = 'd';
					rc5 = 'e';
		        }
		        m = d/q;
		        System.out.println("Victoire en " + m + " coups en moyenne.");
		        break;
			}
			default : System.out.println("Mauvaise saisie.");
		}
	}

}

