/**
 * 
 */
package Improv;
import java.util.Random;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class STD02Ex7bonus2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int r = (int)(Math.random()*100), i = 0, j = 0, g = -1;
		System.out.print("Essaie de deviner le nombre que j'ai choisi. Il est compris entre 1 et 100.\r\nEn combien d'essais penses-tu pouvoir le deviner ? : ");
		j = sc.nextInt();
		if (j <= 0)
			System.out.print("Ne te moque pas de moi.");
		else {
			System.out.print("Bien, tu auras donc " + j + " essais.");
			while (r != g && i < j) {
				System.out.print("\r\nGuess n" + (i+1) + " ? : ");
				i += 1;
				g = sc.nextInt();
				if (r > g)
					System.out.print("Trop petit !");
				else if (g>r)
					System.out.print("Trop grand !");
			}
			if ((i+1) > j)
				System.out.print("\r\nDommage, le nombre était " + r);
			else {
				switch (i) {
					case 1 : System.out.print("Bien joué, le nombre était " + r + "\r\nVictoire en " + i + " coup");
						break;
					default : System.out.print("Bien joué, le nombre était " + r + "\r\nVictoire en " + i + " coups");
						break;
				}
			}
		}
	}

}
