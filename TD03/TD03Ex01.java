/**
 * 
 */
package TD03;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD03Ex01 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int n = 0;
		float g = 0;
		double m = 0;
		System.out.print("Nombre de notes : ");
		n = sc.nextInt();
		if (n < 1)
			System.out.print("Faut pas lancer le programme si tu veux rien faire avec.");
		else {
			for (int i = 0; i<n; i++) {
				System.out.print("Note n�" + (i+1) + " : ");
				g = sc.nextFloat();
				if (g < 0 || g > 20) {
					System.out.println("Sur 20 la note.");
					i--;
				}
				else
					m += g;
			}
			m /= n;
			System.out.print("Moyenne : " + m + "/20");
		}
	}

}
