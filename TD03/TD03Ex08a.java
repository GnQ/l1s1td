/**
 * 
 */
package TD03;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD03Ex08a {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int s = 0;
		System.out.print("Entrer n supérieur à zéro : ");
		int n = sc.nextInt();
		if (n <= 0)
			System.out.println("Supérieur à zéro j'ai dit.");
		else {
			for (int i = 1; i<n; i++) {
				if (n%i == 0) {
					s += i;
				}
			}
			if (s == n)
				System.out.print(n + " est parfait.");
			else
				System.out.print(n + " n'est pas parfait.");
		}
	}
}
