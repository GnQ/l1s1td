/**
 * 
 */
package TD03;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD03Ex08b {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Entrer n supérieur à zéro : ");
		long n = sc.nextLong();
		if (n <= 0)
			System.out.println("Supérieur à zéro j'ai dit.");
		else {
			for (int i = 1; i<n; i++) {
				int s = 0;
				for (int j = 1; j<i; j++) {
					if (i%j == 0) {
						s += j;
					}
				}
				if (s == i)
					System.out.println(i);
			}
		}
	}
}
