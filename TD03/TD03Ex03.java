/**
 * 
 */
package TD03;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD03Ex03 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("n = ");
		int n = sc.nextInt();
		if (n < 0)
			n = -n;
		int d = 10, i = 0;
		while (n != 0) {
			int n2 = n%d;
			System.out.print("10 puissance " + i + " = " + n2 + "\r\n");
			n = n/d;
			i++;
		}
	}

}
