package TD03;
import java.util.Random;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD03Ex09 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int r = (int)(Math.random()*100), i = 0, g = -1;
		System.out.print("Essaie de deviner le nombre que j'ai choisi. Il est compris entre 1 et 100.\r\nTu auras 5 essais.");
		while (r != g && i < 5) {
			System.out.print("\r\nGuess n" + (i+1) + " ? : ");
			i += 1;
			g = sc.nextInt();
			if (r > g)
				System.out.print("Trop petit !");
			else if (g>r)
				System.out.print("Trop grand !");
		}
		if ((i+1) > 5)
				System.out.print("\r\nDommage, le nombre était " + r);
		else {
			switch (i) {
				case 1 : System.out.print("Bien joué, le nombre était " + r + "\r\nVictoire en " + i + " coup");
					break;
				default : System.out.print("Bien joué, le nombre était " + r + "\r\nVictoire en " + i + " coups");
					break;
			}
		}
		
	}

}
