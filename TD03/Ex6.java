/**
 * 
 */
package TD03;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class Ex6 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Valeur minimale en francs : ");
		double fmin = sc.nextDouble();
		System.out.print("Valeur maximale en francs : ");
		double fmax = sc.nextDouble();
		System.out.print("Pas d'incrémentation : ");
		double i = sc.nextDouble();
		if (fmin > fmax)
			System.out.print(fmax + "<" + fmin + "!!!!");
		else {
			for (double f = fmin; f <= fmax; f += i) {
				double e = f/6.55957;
				System.out.println("Valeur en francs : " + f);
				System.out.println("Valeur en euros : " + e);
			}
		}

	}

}
