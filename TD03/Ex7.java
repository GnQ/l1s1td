/**
 * 
 */
package TD03;
import java.util.Random;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class Ex7 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int r = (int)(Math.random()*100), g = -1;
		System.out.print("Essaie de deviner le nombre que j'ai choisi. Il est compris entre 1 et 100.");
		while (r != g) {
			System.out.print("\r\nGuess ? : ");
			g = sc.nextInt();
			if (r > g)
				System.out.print("Trop petit !");
			else if (g>r)
				System.out.print("Trop grand !");
		}
		System.out.print("Bien joué, le nombre était " + r);
	}

}
