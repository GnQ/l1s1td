/**
 * 
 */
package TD03;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD03Ex06 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		double v1 = 0, u1 = 2;
		double v2 = v1, u2 = u1;
		int i = 1;
		System.out.print("Entrer le rang n : ");
		int n = sc.nextInt();
		if (n < 0) {
			System.out.print("Supérieur à zéro !");
		}
		else {
			if (n > 0) {
				for (i = 1; i<=n; i++) {
					v2 = Math.sqrt((1+v1)/2);
					v1 = v2;
					u2 = u1/v2;
					u1 = u2;
				}
			}
			System.out.print("Une approximation de pi au rang " + n + " est : " + u2);
		}
	}

}
