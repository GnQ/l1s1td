/**
 * 
 */
package TD03;
import java.util.Scanner;

/**
 * @author Keziah
 *
 */
public class TD03Ex02 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int i = 0, n = 0;
		float g = 0;
		double m = 0;
		System.out.println("Calcul de moyenne. Entrer les notes une par une. -1 pour afficher le résultat");
			while (g != -1) {
				System.out.print("Note n" + (i+1) + " : ");
				g = sc.nextFloat();
				if ((g < 0 || g > 20) && g != -1) {
					System.out.println("Sur 20 la note.");
				}
				else if (g > -1) {
						i++;
						m += g;
						n += 1;
				}
			}
			m /= n;
			if (n == 1)
				System.out.print("Moyenne : " + m + "/20\r\n" + n + " notes.");
			else
				System.out.print("Moyenne : " + m + "/20\r\n" + n + " notes.");
	}
}
