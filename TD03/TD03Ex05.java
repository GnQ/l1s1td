/**
 * 
 */
package TD03;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class TD03Ex05 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Saisir un entier n : ");
		int n = sc.nextInt();
		int f1 = 1, f2 = 1, f = 1;
		if (n > 0) {
			for (int i = 3; i<=n; i++) {
					f = f1 + f2;
					f1 = f2;
					f2 = f;
			}
			System.out.print("F" + n + " = " + f);
		}
		else 
			if (n < 0)
				System.out.print("Supérieur à 0, j'ai oublié de préciser.");
			else
				System.out.print("F0 = 0");
	}

}
