/**
 * 
 */
package CC1;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class Exercice2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Saisir un nombre de mois : ");
		int m = sc.nextInt();
		if (m<0)
			System.out.println("Erreur, le nombre saisi n'est pas valide.");
		else
			System.out.println("Le capital du Groupe B pour le mois n�" + m + " est : " + capitalB(m) + ".\r\nLe capital du Groupe G"
					+ " pour le mois n�" + m + " est : " + capitalG(m) + ".");
		
		System.out.print("\r\nSaisir un capital : ");
		double c = sc.nextDouble();                               //instance de Scanner --> l. 17
		System.out.println("Il faudra " + moisG(c) + " mois au Groupe G pour atteindre ce capital.");
	}
	public static double capitalB(int mois) {
		double[] tb = new double[(mois+1)]; 				//mois+1 car on compte nb de mois + mois 0
		tb[0] = 10000;
		if(tb.length > 1)
			tb[1] = 30000;									//if n�cessaire pour �viter l'erreur OutOfBounds si mois = 0.
		for (int i=2; i<tb.length; i++) {
			tb[i] = (tb[i-1]*1.2) + (tb[i-2]*1.4);
		}
		return tb[(mois)]; 							//utilisation d'un tableau pour �viter l'utilisation de if si mois = 0 ou mois = 1
	}
	public static double capitalG(int mois) {
		double[] tg = new double[(mois+1)]; 				//mois+1 car on compte nb de mois + mois 0
		tg[0] = 60000;
		for (int i=1; i<tg.length; i++) {
			tg[i] = (tg[i-1]*1.1) + (0.4*capitalB(mois));   //appel de la fonction capitalB pour obtenir Bn
		}
		return tg[(mois)];							 //utilisation d'un tableau pour �viter l'utilisation de if si mois = 0 ou mois = 1
	}
	public static int moisG(double capital) {
		int i = 0;
		while (capitalG(i) < capital) {
			i++;											 //on passe tous les mois jusqu'� d�passement du seuil (valeur de "capital")
		}
		return i;
	}
}
