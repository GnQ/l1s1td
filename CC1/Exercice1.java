/**
 * 
 */
package CC1;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class Exercice1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Entrer l'entier n : ");
		int n = sc.nextInt();
		System.out.println("n = " + n);				//affichage de n (question 1)
		
		int gd = n;
		for (int i=2; i<=(n/2); i++) {				//test de tous les diviseurs de n (modulo = 0)
			if ((n%i)==0)
				gd = i;
		}
		System.out.println("Le plus grand diviseur de n est : " + gd); //affichage du plus grand diviseur de n (question 2)
		
		System.out.print("Entrer l'entier m : ");	
		int m = sc.nextInt();						//instance de Scanner --> l. 17
		if ((n*m)>1000)								//Le plus petit multiple de n et m est n*m
			System.out.println("pas de multiple");
		else
			System.out.println("Le plus petit multiple commun de n et m entre 0 et 1000 est : " + (n*m)); 
		//affichage du plus petit multiple commun de n et m compris entre 0 et 1000 (question 3)
	}

}
