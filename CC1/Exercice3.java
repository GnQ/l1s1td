/**
 * 
 */
package CC1;
import java.util.Scanner;
/**
 * @author Keziah
 *
 */
public class Exercice3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.print("Saisir un nombre n d'étudiants et un nombre n de notes :\r\n n = ");
		int n = sc.nextInt();
		System.out.print("m = ");
		int m = sc.nextInt();
		float[][] mat = new float[n][m];
		saisie(mat);
		moyennes(mat);
	}
	public static float[][] saisie(float[][] tab) {
		Scanner sc = new Scanner(System.in);
		for(int i=0; i<tab.length; i++) {
			for (int j=0; j<tab[0].length; j++) {
				System.out.print("Note n" + (j+1) + " de l'étudiant " + i + " : ");
				tab[i][j] = sc.nextFloat();
			}
		}
		return tab;
	}
	public static float minTab(float[] tab) {
		float min = tab[0];
		for (int i=1; i<tab.length; i++) {
			if (tab[i] < min)
				min = tab[i];
		}
		return min;
	}
	public static double moyenneMoinsMin(float[] tab) {
		float m = 0;
		int o = 0;								//vrifie que le nombre de notes soit exact (au cas o le minimum est prsent plusieurs fois)
		for (int i=0; i<tab.length; i++) {
			if (tab[i] != minTab(tab)) {
				m += tab[i];
				o += 1;
			}
		}
		if (o != tab.length-1)
			m += (tab.length-1-o)*minTab(tab);	 //rajout aux notes du nombre de notes gales au minimum retires par la boucle for
		m /= tab.length-1;
		return m;
	}
	public static void moyennes(float[][] notes) {
		double[] moy = new double[notes.length];     //cration d'un tableau des moyennes de chaque tudiant
		for (int i=0; i<notes.length; i++) {
			moy[i] = moyenneMoinsMin(notes[i]);
			System.out.println("Moyenne de l'étudiant n°" + (i+1) + " : " + moy[i]);
		}
		System.out.println("Moyenne de la classe : " + moyenne(moy));
	}
	public static double moyenne(double[] moy) {
		double m = 0;
		for (int i=0; i<moy.length; i++) {
			m += moy[i];
		}
		m /= moy.length;
		return m;
	}
}
